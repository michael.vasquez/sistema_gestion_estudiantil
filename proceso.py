import sqlite3


class EstudiantesUnl:
    cedula = str
    nombre = ''
    correo = ''

    def __init__(self):
        self.dbConnection = sqlite3.connect('BD_estudiante.sqlite')
        self.cursor = None

    def sql_tabla(self):
        self.dbCursor = self.dbConnection.cursor()
        self.dbCursor.execute('''CREATE TABLE estudiantes (
            cedula TEXT(10) PRIMARY KEY UNIQUE,
            nombre TEXT(100),
            correo TEXT)
            ''')
        self.dbCursor.close()

    def matricular_estudiante(self):
        print('Rellene todos los datos para proceder con la matricula\n')

        cedula = input('Ingrese su número de cédula: ')
        nombre = input('Ingrese su nombre: ')
        correo = input('Ingrese su correo electrónico: ')

        self.dbCursor = self.dbConnection.cursor()
        self.dbCursor.executemany("INSERT INTO estudiantes(cedula, nombre, correo) VALUES (?, ?, ?)",
                                  (cedula, nombre, correo))
        self.dbConnection.commit()
        print('Su matricula se ha guardado exitosamente')
        repetir = input('\n¿Desea realizar otra operación? (S/n): ')
        if repetir == 'S' or repetir == 's':
            self.menu()
        elif repetir == 'N' or repetir == 'n':
            print('Hasta luego')
            self.dbConnection.close()

    def ver_datos(self):
        print('\tESTUDIANTES MATRICULADOS\n')
        self.dbCursor = self.dbConnection.cursor()
        self.dbCursor.execute("SELECT * FROM estudiantes")
        rows = self.dbCursor.fetchall()
        cont = 1
        for i in rows:
            if cont <= len(i):
                print(f'ESTUDIANTE {cont}')
                print('Cédula:\t', i[0], '\nNombre:\t', i[1], '\nCorreo:\t', i[2])
                cont += 1
        repetir = input('\n¿Desea realizar otra operación? (S/n): ')
        if repetir == 'S' or repetir == 's':
            self.menu()
        elif repetir == 'N' or repetir == 'n':
            print('Hasta luego')
            self.dbConnection.close()

    def consultar(self):
        cedula = str(input('Ingrese el número de cédula del estudiante: '))
        self.dbCursor = self.dbConnection.cursor()
        self.dbCursor.execute("SELECT * FROM estudiantes WHERE cedula = (?)", (cedula,))
        rows = self.dbCursor.fetchall()
        for i in rows:
            print(i)
        repetir = input('\n¿Desea realizar otra operación? (S/n): ')
        if repetir == 'S' or repetir == 's':
            self.menu()
        elif repetir == 'N' or repetir == 'n':
            print('Ha salido')
            self.dbConnection.close()

    def borrar_estudiante(self):
        cedula = input('Ingrese el número de cédula a eliminar: ')
        self.dbCursor = self.dbConnection.cursor()
        self.dbCursor.execute("DELETE FROM estudiantes WHERE cedula = (?)", (cedula,))
        self.dbConnection.commit()
        repetir = input('\n¿Desea realizar otra operación? (S/n): ')
        if repetir == 'S' or repetir == 's':
            self.menu()
        elif repetir == 'N' or repetir == 'n':
            print('Hasta luego')
            self.dbConnection.close()

    def menu(self):
        print('\n¿Qué proceso desea realizar?')
        print('1 --> Matricula')
        print('2 --> Mostrar registro')
        print('3 --> Buscar estudiante')
        print('4 --> Eliminar estudiante')
        print('Digite el número según la acción que corresponda\t')

        op = int(input(''))

        if op == 1:
            self.matricular_estudiante()
        elif op == 2:
            self.ver_datos()
        elif op == 3:
            self.consultar()
        elif op == 4:
            self.borrar_estudiante()
        else:
            print('\tERROR!!\nValor fuera de rango')


if __name__ == '__main__':
    a = EstudiantesUnl()
    # a.sql_tabla()
    a.menu()
